<?php

namespace Drupal\apidae_tourisme\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\apidae_tourisme\ApidaeSync;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ApidaeController.
 */
class ApidaeController extends ControllerBase {

  /**
   * Drupal\apidae_tourisme\ApidaeSync definition.
   *
   * @var \Drupal\apidae_tourisme\ApidaeSync
   */
  protected $apidaeTourismeSync;

  /**
   * Constructs a new ApidaeController object.
   */
  public function __construct(ApidaeSync $apidae_tourisme_sync) {
    $this->apidaeTourismeSync = $apidae_tourisme_sync;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('apidae_tourisme.sync')
    );
  }

  /**
   * Force sync of a ObjetTourstique node.
   */
  public function sync($objet) {
    $objet = Node::load($objet);
    if ($objet->getType() === 'objet_touristique') {
      $this->apidaeTourismeSync->sync(TRUE, [$objet->get('field_id_ws')->value]);
    }
    return new RedirectResponse(Url::fromRoute('view.objets_tourisques.page')->toString());
  }

}
