<?php

namespace Drupal\apidae_tourisme\Commands;

use Drupal\apidae_tourisme\ApidaeSync;
use Drush\Commands\DrushCommands;

class Apidae extends DrushCommands {

  /**
   * @var \Drupal\apidae_tourisme\ApidaeSync
   */
  protected $apidaeSync;

  public function __construct(ApidaeSync $apidaeSync) {
    parent::__construct();
    $this->apidaeSync = $apidaeSync;
  }

  /**
   * Launch Apidae Sync
   *
   * @usage drush apidae:sync
   *   Launch a regular sync
   *
   * @usage drush apidae:sync --force-update
   *   Launch a sync, and force an update
   *
   * @command apidae:sync
   * @aliases apis
   */
  public function sync($options = ['force-update' => FALSE]) {
    $force_update = (bool) $options['force-update'];
    $this->apidaeSync->sync($force_update);
    $this->logger()->success(dt('Apidae sync over'));
  }
}
